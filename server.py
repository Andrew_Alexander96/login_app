from flask import Flask, render_template, request, redirect, session
from db import find_user

app = Flask(__name__)
app.secret_key = 'nf92h93rr30'


@app.route('/')
def hello():
    return render_template('home.html')


@app.route('/dashboard')
def dashboard():
    if 'username' in session:
        user = session['username']
        return render_template('home.html', utilizator=user)
    else:
        return redirect('/')


@app.route('/login', methods=['GET', 'POST'])
def login():
    username = request.form['user']
    password = request.form['pass'].encode('UTF-8')

    user_tup = find_user(username)
    if user_tup:
        hashed_password = user_tup[2].encode('UTF-8')

        if bcrypt.checkpw(password, hashed_password):
            session['username'] = username
            return redirect('/dashboard')

    return render_template('login.html', msg='Invalid credentials')


@app.route('/logout')
def logout():
    session.clear()
    return render_template('login.html', msg='Signed out')


if __name__ == '__main__':
    app.run(debug=True)
