import sqlite3
import bcrypt


SQL_CREATE = '''
    CREATE TABLE IF NOT EXISTS users(
    id INTEGER AUTOINCREMENT PRIMARY KEY,
    username  VARCHAR(200) NOT NULL UNIQUE,
    password VARCHAR(200) NOT NULL
    )
'''

SQL_SELECT = '''
     SELECT * FROM users WHERE username = ?
'''

SQL_INSERT = '''
     INSERT INTO users (username, password) VALUES(?, ?)
'''


def find_user(username):
    with sqlite3.connect('database.db') as conn:
        curs = conn.cursor()
        curs.execute(SQL_CREATE, (username,))
        rows = curs.fetchall()
        if rows:
            return rows[0]
    return None


if __name__ == '__main__':
    with sqlite3.connect('database.db') as conn:
        curs = conn.cursor()
        curs.execute(SQL_CREATE)
        if not find_user('admin'):
            password = b'letmein'
            hashed = bcrypt.hashpw(password, bcrypt.gensalt())
            curs.execute(SQL_INSERT, ('adminx', hashed))
